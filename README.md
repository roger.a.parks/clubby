# Clubby The Seal

A platformer game utilizing HTML5 canvas.

Latest Release can be found at http://roger.a.parks.gitlab.io/clubby/

![Penguin and Seal in the Club](img/theClub.svg)

## Game Play

The player advances through the game by visiting map screens.
Certain map screens contain gates which will require the player to defeat some number of seals before advancing. 
The player defeats seals using the player's weapon. 
The player's health is reduced when the player collides with seals. 
When the player's health is depleted, the game will reset to the previous gate.
After the last screen has been visited, the game is complete.

### Controls

| Action      | Key                 | Alternate Key 1 | Alternate Key 2 | Alternate Key 3 | Alternate Key 4 |
|-------------|---------------------|-----------------|:----------------|:----------------|-----------------|
| Right       | ``→`` (right arrow) | ``l``           | ``d``           |                 |                 |
| Left        | ``←`` (left arrow)  | ``h``           | ``a``           |                 |                 |
| Jump        | ``↑`` (up arrow)    | ``k``           | ``w``           | ``␣`` (space)   |                 |
| Weapon      | ``↓`` (down arrow)  | ``j``           | ``s``           | ``⌃`` (control) | ``x``           |
| Reload Game | ``r``               |                 |                 |                 |                 |


### Resume Codes

Resume codes are used to manipulate gameplay and act much like cheat codes.
They are helpful during development.
To enter a Resume Code, type ``:`` then a code and argument from the following table.
Note that multiple codes can be chained together like ``:g1h90``.
When a resume code is entered a message is printed out in the bottom right hand corner.

| Code  | Arguments        | Example   | Description                                                           |
|-------|------------------|-----------|-----------------------------------------------------------------------|
| ``g`` | map_screen_index | ``:g2``   | Goto map screen index 2 (3rd map screen). Score is automatically set. |
| ``s`` | score            | ``:s20``  | Score is set to 20.                                                   |
| ``h`` | health           | ``:h100`` | Health is set to 100.                                                 |

## Todo

### Bugs

* The player's height is instantly increased when jumping against a wall that has a slope on top of it.
 
### Ideas

* Build Updates:
    * During the build write build version to version.html.
    * During the build write converted readme.md to readme.html.
    * Make initial version.hml and readme.html explaining not to modify since they are populated by the build.
* Resume Codes:
    * Every time a gate is passed...
        * Message out current resume code.
        * Update current URL # resume code.
        * Put resume link on page with URL # resume code.
* Weapons:
    * Reduce baddie health for every update where the weapon is colliding.
        * Give baddies health so weapons can reduce it.
        * Update weapon to reduce baddie health.
    * Different weapons can be collected on map.
        * Add new weapon sprites.
    * Different weapons reduce baddie health faster.
    * Weapon Upgrades:
        * ``0`` is starting club.
        * ``1`` can club while moving and jumping.
        * ``2`` is a longer club.
        * ``3`` is a club that is thrown.
* Baddies:
    * Seals of different levels are distinguished by color and size.
    * Exploding seals spawn more seals when defeated.
* Snow Particles:
    * Snow particles constantly fall throughout level.
    * Snow particles stop on the first ledge they hit.
    * Snow particles get x velocity when weapon is used.
    * Snow particles get x and y velocity when they are walked over.
    * There is a maximum number of snow particle that can accumulate per length.

## Development

### Sprite Images

Sprite image assets are drawn in inkscape, and are saved as ``svg`` files. The ``svg`` files 
are exported to ``png`` by imagemagick using the ``magick`` command specified in 
``package.json``. For easy of development run ``npm install nodemon`` then ``npm run exportpng`` 
to run a script that will make new ``png`` files when changes to any of the ``svg`` files are 
detected. 

### Tests

The command ``npm run test`` can be used to run unit tests a single time. This is the command that the 
CI job runs this on commit. 

During develop, use a Jest IntelliJ Run/Debug Configuration to run tests continuously locally. The 
configuration below will run test every time a tested source file is changed.

| IntelliJ Run/Debug Configuration Parameter | Value                         |
|--------------------------------------------|-------------------------------|
| Node options                               | ``--experimental-vm-modules`` |
| Jest options                               | ``--watch``                   |


### Supporting Software

| Software    | Chocolatey Install Command                          |
|-------------|-----------------------------------------------------|
| Imagemagick | ``choco install imagemagick --version 7.1.1.600``   |
| InkScape    | ``choco install InkScape --version 1.2.2.20230220`` |
| nodejs      | ``choco install nodejs --version 19.8.1``           |

---

See [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for instructions on how to markdown.
