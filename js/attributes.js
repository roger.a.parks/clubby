"use strict";

export class AttributeXY {
    constructor(attributeX = 0, attributeY = 0) {
        this.x = attributeX;
        this.y = attributeY;
    }
}

export class AttributeFourDirection {
    constructor() {
        this.right = false;
        this.left = false;
        this.up = false;
        this.down = false;
    }

    any() {
        return this.right || this.left || this.up || this.down
    }
}