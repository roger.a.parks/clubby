"use strict";

export const AudioEnum = {
    CLUB: "audio/club.mp3",
    CLUBBED: "audio/clubbed.mp3",
    JUMP: "audio/jump.mp3",
    DAMAGE: "audio/damage.mp3",
    OVER: "audio/over.mp3",
    WHISTLE: "audio/whistle.mp3",
}

export function playAudio(audioPath, onEnded = null) {
    let audio = new Audio(audioPath)
    if (onEnded !== null) {
        audio.addEventListener('ended', onEnded)
    }
    audio.play()
}
