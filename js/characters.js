import {
    BackSlopeMapTile,
    Baddie,
    ForwardSlopeMapTile,
    InnerMapTile,
    Player,
    SquareMapTile
} from "./gameElements.js";
import {AttributeXY} from "./attributes.js";

class Sprite {

    constructor(gameElementInstance) {
        this.gameElementInstance = gameElementInstance

        this.cvs = document.createElement("canvas")
        this.cvs.width = this.gameElementInstance.size.x
        this.cvs.height = this.gameElementInstance.size.y
        this.ctx = this.cvs.getContext("2d");
        this.ctx.imageSmoothingEnabled = false

        document.body.appendChild(this.cvs)
        document.body.appendChild(Object.assign(document.createElement("span"), {
            innerText: gameElementInstance.constructor.name,
            className: "characterName"
        }))
        document.body.appendChild(document.createElement("br"))

    }

    update() {
        this.blankCanvas();
        this.gameElementInstance.draw(this.cvs, this.ctx)
    }

    blankCanvas() {
        this.ctx.fillStyle = "black";
        this.ctx.fillRect(0, 0, this.cvs.width, this.cvs.height);
    }
}

function update(sprites) {
    sprites.forEach(
        sprite => {
            sprite.update();
        }
    )
}

export function animateSprites() {
    window.onload = function () {
        let sprites = [
            new Sprite(new Player(0, 0)),
            new Sprite(Object.assign(new Player(0, 0), {
                previousDirectionWasRight: false
            })),
            new Sprite(Object.assign(new Player(0, 0), {
                velocity: new AttributeXY(1, 0)
            })),
            new Sprite(Object.assign(new Player(0, 0), {
                previousDirectionWasRight: false,
                velocity: new AttributeXY(1, 0)
            })),
            new Sprite(Object.assign(new Player(0, 0), {
                velocity: new AttributeXY(0, 1)
            })),
            new Sprite(Object.assign(new Player(0, 0), {
                previousDirectionWasRight: false,
                velocity: new AttributeXY(0, 1)
            })),
            new Sprite(Object.assign(new Player(0, 0), {
                isTakingDamageAnimationStep: 1
            })),
            new Sprite(new Baddie(0, 0)),
            new Sprite(new Baddie(0, 0, false)),
            new Sprite(new SquareMapTile(0, 0)),
            new Sprite(new InnerMapTile(0, 0)),
            new Sprite(new ForwardSlopeMapTile(0, 0)),
            new Sprite(new BackSlopeMapTile(0, 0)),
        ]

        setInterval(update, 1000 / 40, sprites);
    }
}
