"use strict";

import {Game} from "./game.js";
import {
    BackSlopeMapTile,
    Baddie,
    ForwardSlopeMapTile,
    Gate,
    InnerMapTile,
    Player,
    SquareMapTile
} from "./gameElements.js";

let game = new Game();
let cvs = document.getElementById("gc");
let ctx = cvs.getContext("2d");

function keyDown(evt) {
    if (game.resumeCode === "") {
        if ((evt.code === "ArrowRight") || (evt.key === "l") || (evt.key === "d")) {
            game.player.actionRequested.right = true;
        } else if ((evt.code === "ArrowLeft") || (evt.key === "h") || (evt.key === "a")) {
            game.player.actionRequested.left = true;
        } else if ((evt.code === "ArrowUp") || (evt.key === " ") || (evt.key === "k") || (evt.key === "w")) {
            game.player.actionRequested.up = true;
        } else if (
            (evt.code === "ArrowDown") ||
            (evt.key === "Control") ||
            (evt.key === "j") ||
            (evt.key === "s") ||
            (evt.key === "x")
        ) {
            game.player.actionRequested.down = true;
        } else if ((evt.key === ":") && !game.isComplete) {
            game.resumeCode = ":"
            game.messageGeneral = game.resumeCode
            game.draw(ctx, cvs)
        } else if (evt.key === "r") {
            document.location.reload();
        } else if (evt.key === "c") {
            game.isComplete = true
        }
    } else {
        if (evt.code === "Enter") {
            game.applyResumeCode();
        } else {
            if (evt.altKey || evt.ctrlKey || evt.metaKey || evt.shiftKey) {
                // eat these keys
            } else if (evt.key === "Escape") {
                game.resumeCode = ""
            } else if (evt.key === "Backspace") {
                game.resumeCode = game.resumeCode.slice(0, -1)
            } else {
                game.resumeCode += evt.key;
            }
            game.messageGeneral = game.resumeCode
            game.draw(ctx, cvs)
        }
    }
}

function keyUp(evt) {
    if ((evt.code === "ArrowRight") || (evt.key === "l") || (evt.key === "d")) {
        game.player.actionRequested.right = false;
    } else if ((evt.code === "ArrowLeft") || (evt.key === "h") || (evt.key === "a")) {
        game.player.actionRequested.left = false;
    } else if ((evt.code === "ArrowUp") || (evt.key === " ") || (evt.key === "k") || (evt.key === "w")) {
        game.player.actionRequested.up = false;
    } else if (
        (evt.code === "ArrowDown") ||
        (evt.key === "Control") ||
        (evt.key === "j") ||
        (evt.key === "s") ||
        (evt.key === "x")
    ) {
        game.player.actionRequested.down = false;
    }
}

function update(game) {
    try {
        if (game.hasFocus && (game.resumeCode === "") && !game.isComplete) {
            // load screen if screen has changed
            if (game.mapScreenIndex !== game.mapScreenIndexPrevious) {
                game.elements = game.elements.filter( // remove all non-player map tiles
                    element => (element instanceof Player))

                let mapRow, mapCol;
                let mapRowMax = game.mapScreens[game.mapScreenIndex].mapRows.length
                let mapColMax = game.mapScreens[game.mapScreenIndex].mapRows[0].length;
                for (mapRow = 0; mapRow < mapRowMax; mapRow++) {
                    for (mapCol = 0; mapCol < mapColMax; mapCol++) {
                        const mapCellContents = game.mapScreens[game.mapScreenIndex].mapRows[mapRow][mapCol];
                        // add baddies from map
                        if ((mapCellContents === "s") || (mapCellContents === "S")) {
                            let baddieHash = Baddie.getBaddieHash(game.mapScreenIndex, mapRow, mapCol)
                            if (game.baddiesClubbed.indexOf(baddieHash) === -1) {  // don't show previously clubbed
                                let baddieLeftX;
                                let previousDirectionWasRight;
                                if (mapCellContents === "s"){
                                    baddieLeftX = ((1 + mapCol) * 50) - new Baddie(0, 0).size.x;
                                    previousDirectionWasRight = true;
                                } else {
                                    baddieLeftX = ((mapCol) * 50);
                                    previousDirectionWasRight = false;
                                }
                                game.elements.push(
                                    new Baddie(
                                        baddieLeftX,
                                        ((1 + mapRow) * 50) - new Baddie(0, 0).size.y,
                                        previousDirectionWasRight,
                                        baddieHash
                                    ))
                            }
                        }
                        // add map tiles
                        if (mapCellContents === "■") {
                            game.elements.unshift(new SquareMapTile(
                                mapCol * 50,
                                mapRow * 50
                            ))
                        }
                        if (mapCellContents === "□") {
                            game.elements.unshift(new InnerMapTile(
                                mapCol * 50,
                                mapRow * 50
                            ))
                        }
                        if (mapCellContents === "↗") {
                            game.elements.unshift(new ForwardSlopeMapTile(
                                mapCol * 50,
                                mapRow * 50
                            ))
                        }
                        if (mapCellContents === "↘") {
                            game.elements.unshift(new BackSlopeMapTile(
                                mapCol * 50,
                                mapRow * 50
                            ))
                        }
                        if (mapCellContents === "g") {
                            game.elements.unshift(new Gate(
                                ((1 + mapCol) * 50) - new Gate(0, 0).size.x,
                                ((1 + mapRow) * 50) - new Gate(0, 0).size.y,
                            ))
                        }
                    }
                }

                game.mapScreenIndexPrevious = game.mapScreenIndex;
                game.messageMapScreen = `Map Screen ${game.mapScreenIndex}`
                if (game.messageGeneral.indexOf("Map: ") === -1) {  // don't map set message
                    game.messageGeneral = ""
                }


                game.messageNextGateScore = ""
                for (const map of game.mapScreens.slice(game.lastGateMapScreenIndex)) {
                    if (map.hasRequiredScoreToAdvance){
                        game.messageNextGateScore = `Next Gate Score ${map.requiredScoreToAdvance}`
                        break;
                    }
                }
            }

            if (game.health > 0) {
                game.elements.forEach(element => element.update(game))
            }

            game.draw(ctx, cvs)
        } else {
            // stop clubbing when focus is loss (i.e. <ctrl> + <tab>)
            game.player.actionRequested.down = false;
        }
        if (game.isComplete){
            game.complete(ctx, cvs)
        }
    } catch (error) {
        console.log("Caught error in update loop. Bailing out...")
        console.log(error);
        clearInterval(game.updateRunner)
    }
}

export function runGame() {
    window.onload = function () {
        window.addEventListener("keydown", keyDown);
        window.addEventListener("keyup", keyUp);
        // pause when focus is lost
        cvs.addEventListener("mouseenter", () => {
            game.hasFocus = true;
        })
        cvs.addEventListener("mouseleave", () => {
            game.hasFocus = false;
        })

        ctx.imageSmoothingEnabled = false
        update(game)
        game.updateRunner = setInterval(update, 1000 / 40, game);
    };
}
