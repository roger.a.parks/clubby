// noinspection JSUnresolvedReference,JSUnresolvedFunction

"use strict";

import {Player, TestPixel, ForwardSlopeMapTile, BackSlopeMapTile} from "./gameElements.js";

test("Test Right Boundary", () => {
    let player = new Player(0,0)

    expect(player.isCollidingWith(
        new TestPixel(player.size.x, 0)
    ).any()).toBe(false);
    expect(player.isCollidingWith(
        new TestPixel(player.size.x - 1, 0)
    ).any()).toBe(true);

    expect(player.isCollidingWith(
        new TestPixel(player.size.x, player.size.y - 1)
    ).any()).toBe(false);
    expect(player.isCollidingWith(
        new TestPixel(player.size.x -1, player.size.y - 1)
    ).any()).toBe(true);
})

test("Test Upper Boundary", () => {
    let player = new Player(0,0)

    expect(player.isCollidingWith(
        new TestPixel(0, -1)
    ).any()).toBe(false);
    expect(player.isCollidingWith(
        new TestPixel(0, 0)
    ).any()).toBe(true);

    expect(player.isCollidingWith(
        new TestPixel(player.size.x - 1, -1)
    ).any()).toBe(false);
    expect(player.isCollidingWith(
        new TestPixel(player.size.x - 1, 0)
    ).any()).toBe(true);
})

test("Test Forward Sloped", () => {
    let forwardSlopeMapTile = new ForwardSlopeMapTile(0, 0)

    // bottom left
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(0, 49)
    ).any()).toBe(true);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(-1, 49)
    ).any()).toBe(false);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(0, 48)
    ).any()).toBe(false);

    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(1, 48)
    ).any()).toBe(true);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(1, 47)
    ).any()).toBe(false);

    // top right
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(49, 0)
    ).any()).toBe(true);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(50, 0)
    ).any()).toBe(false);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(49, -1)
    ).any()).toBe(false);

    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(48, 1)
    ).any()).toBe(true);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(48, 0)
    ).any()).toBe(false);

    // bottom right
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(49, 49)
    ).any()).toBe(true);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(50, 49)
    ).any()).toBe(false);
    expect(forwardSlopeMapTile.isCollidingWith(
        new TestPixel(49, 50)
    ).any()).toBe(false);
})

test("Test Back Sloped", () => {
    let backSlopeMapTile = new BackSlopeMapTile(0, 0)

    // top left
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(0, 0)
    ).any()).toBe(true);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(-1, 0)
    ).any()).toBe(false);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(0, -1)
    ).any()).toBe(false);

    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(1, 1)
    ).any()).toBe(true);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(1, 0)
    ).any()).toBe(false);

    // bottom right
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(49, 49)
    ).any()).toBe(true);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(50, 49)
    ).any()).toBe(false);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(49, 48)
    ).any()).toBe(false);

    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(48, 48)
    ).any()).toBe(true);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(48, 47)
    ).any()).toBe(false);

    // bottom left
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(0, 49)
    ).any()).toBe(true);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(-1, 49)
    ).any()).toBe(false);
    expect(backSlopeMapTile.isCollidingWith(
        new TestPixel(0, 50)
    ).any()).toBe(false);

})
