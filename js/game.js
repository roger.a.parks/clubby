"use strict";

import {mapScreens} from "./levels.js";
import {AttributeXY} from "./attributes.js";
import {Baddie, Player} from "./gameElements.js";

export class Game {
    player = new Player(0, 0)
    elements = [this.player];
    baddiesClubbed = [];
    gravity = true
    walls = true

    mapScreens = mapScreens
    mapScreenInitial = 0
    mapScreenIndex = this.mapScreenInitial
    mapScreenIndexPrevious = -1

    updateRunner = undefined;
    hasFocus = true;
    resumeCode = "";
    isComplete = false;

    healthMax = 100;
    health = this.healthMax;
    score = 0;

    messageGeneral = "";
    messageMapScreen = "";
    messageNextGateScore = "";

    lastGateMapScreenIndex = 0;
    lastGateScore = 0;
    lastGateBaddiesClubbed = [];

    constructor() {
        this.mapScreens.validateMapScreens()
        this.reset()
    }

    reset() {
        this.player.previousDirectionWasRight = true
        this.player.velocity = new AttributeXY();
        this.health = this.healthMax
        this.mapScreenIndexPrevious = -1

        // Restore last gate parameters
        this.setMapScreenIndex(this.lastGateMapScreenIndex);
        this.score = this.lastGateScore;
        this.baddiesClubbed = this.lastGateBaddiesClubbed.slice(); // use slice to get new array
    }

    setMapScreenIndex(mapScreenIndex, setPosition = true, playerIsOnLeft = true){
        this.mapScreenIndex = mapScreenIndex
        if (setPosition) {
            this.player.velocity.y = 0
            if (playerIsOnLeft) {
                this.player.topLeft.x = 0
                this.player.topLeft.y = this.mapScreens[mapScreenIndex].getGround(0) - this.player.size.y - 1;
                this.player.previousDirectionWasRight = true
            } else {
                let maxColumn = this.mapScreens[mapScreenIndex].mapRows[0].length - 1
                this.player.topLeft.x = (maxColumn * 50) - this.player.size.x;
                this.player.topLeft.y = this.mapScreens[mapScreenIndex].getGround(maxColumn) - this.player.size.y - 1;
                this.player.previousDirectionWasRight = false
            }
        }
    }

    complete(ctx, cvs) {
        clearInterval(this.updateRunner)
        let drawDelayMs = 30;
        let rowMax = Math.floor(cvs.height / this.player.size.y);
        let colMax = Math.floor(cvs.width / this.player.size.x);
        for (let row = 0; row < rowMax; row++) {
            for (let col = 0; col < colMax; col++) {
                if ((
                        ((row < 8) || (row > 11)) ||
                        ((col < 4) || (col >= (colMax - 4)))
                    ) && (row < (rowMax - 2))
                ) {
                    setTimeout(() => {
                        this.elements.push(new Player(
                            col * this.player.size.x,
                            row * this.player.size.y
                        ))
                        if ((row % 2) === 0) {
                            this.elements[this.elements.length - 1].previousDirectionWasRight = false;
                        }
                        this.draw(ctx, cvs)
                    }, drawDelayMs * (row + col));
                }
            }
        }
        setTimeout(() => {
            ctx.textAlign = "center";
            ctx.fillStyle = "#fb0000"
            ctx.font = "bold 70px Courier";
            ctx.fillText("Congratulations!", cvs.width / 2, cvs.height / 2)
            ctx.textAlign = "left";
        }, (drawDelayMs * (rowMax + colMax)) + 300)
        setTimeout(() => {
            ctx.textAlign = "center";
            ctx.fillStyle = "#dd00ff"
            ctx.font = "bold 30px Courier";
            ctx.fillText("You have completed the game", cvs.width / 2, (cvs.height / 2) + 30)
            ctx.textAlign = "left";
        }, (drawDelayMs * (rowMax + colMax)) + 600)
    }

    drawHealth(cvs, ctx) {
        // draw health
        let healthWidth = 150;
        let healthHeight = 20;
        let healthX = 10
        let healthY = cvs.height - 10 - healthHeight;
        ctx.fillStyle = "lime"
        ctx.fillRect(
            healthX, healthY,
            healthWidth * (this.health / this.healthMax), healthHeight)
        ctx.beginPath()
        ctx.strokeStyle = "Black";
        ctx.lineWidth = 3;
        ctx.rect(healthX, healthY, healthWidth, healthHeight)
        ctx.stroke();
        ctx.font = "15px Courier";
        ctx.fillStyle = "Black";
        ctx.fillText(
            this.health.toString(),
            healthX + ctx.lineWidth,
            healthY - ctx.lineWidth + (healthHeight / 2) + (15 / 2));
    }

    applyResumeCode(){
        let codeMessages = []

        const mapScreenMatches = this.resumeCode.match(/g(\d+)/)
        if (mapScreenMatches){
            let desiredLevel = parseInt(mapScreenMatches[1])
            if ((desiredLevel < this.mapScreens.length) && (desiredLevel >= 0)) {
                this.setMapScreenIndex(desiredLevel);
                this.lastGateMapScreenIndex = this.mapScreenIndex

                // add all encountered baddies to baddies clubbed and score when ``g`` resume code is used
                this.baddiesClubbed = [];
                this.score = 0;
                    this.mapScreens.slice(0, desiredLevel).forEach((mapScreen, mapScreenIndex) => {
                    let mapRow, mapCol;
                    let mapRowMax = this.mapScreens[this.mapScreenIndex].mapRows.length
                    let mapColMax = this.mapScreens[this.mapScreenIndex].mapRows[0].length;
                    for (mapRow = 0; mapRow < mapRowMax; mapRow++) {
                        for (mapCol = 0; mapCol < mapColMax; mapCol++) {
                            const mapCellContents = mapScreen.mapRows[mapRow][mapCol];
                            if ((mapCellContents === "s") || (mapCellContents === "S")) {
                                // add baddie hashes
                                let baddieHash = Baddie.getBaddieHash(mapScreenIndex, mapRow, mapCol)
                                this.baddiesClubbed.push(baddieHash);
                                this.lastGateBaddiesClubbed = this.baddiesClubbed.slice(); // use slice to get new array
                                // add baddie to score
                                this.score += 1;
                                this.lastGateScore = this.score;
                            }
                        }
                    }
                });

                codeMessages.push(`Map: ${this.mapScreenIndex}`)
            } else {
                codeMessages.push('Map: Invalid')
            }
        }
        const scoreMatches = this.resumeCode.match(/s(\d+)/)
        if (scoreMatches){
            this.score = parseInt(scoreMatches[1]);
            this.lastGateScore = this.score
            codeMessages.push(`Score: ${this.score}`)
        }

        const healthMatches = this.resumeCode.match(/h(\d+)/)
        if (healthMatches){
            this.health = parseInt(healthMatches[1]);
            codeMessages.push(`Health: ${this.health}`)
        }

        if (codeMessages.length !== 0) {
            this.messageGeneral = `Set ${codeMessages.join(", ")}`
        } else{
            this.messageGeneral = `Unrecognized resume code: '${this.resumeCode}'`
        }
        this.resumeCode = "";
    }

    draw(ctx, cvs){
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, cvs.width, cvs.height);
        this.elements.forEach(element => element.draw(cvs, ctx))

        // draw additional items that are contained in Game class
        this.drawHealth(cvs, ctx);
        this.drawScore(ctx, cvs);
        this.drawMessages(ctx, cvs);
    }

    drawScore(ctx, cvs) {
        // draw clubbed
        ctx.fillText(
            "Score: " + this.score.toString(),
            10,
            cvs.height - 35
        )
    }

    drawMessages(ctx, cvs) {
        ctx.textAlign = 'right';
        ctx.fillText(
            this.messageGeneral,
            cvs.width - 10,
            cvs.height - 10
        );
        ctx.textAlign = 'left';

        ctx.fillText(
            this.messageMapScreen,
            173,
            cvs.height - 22
        );
        ctx.fillText(
            this.messageNextGateScore,
            173,
            cvs.height - 10
        );
    }
}
