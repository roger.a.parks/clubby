"use strict";

import {AttributeFourDirection, AttributeXY} from "./attributes.js";
import {AudioEnum, playAudio} from "./audio.js";

function getNCollidingSquareMapTiles(newPositionColliding) {
    let nSquareMapTiles = 0
    newPositionColliding.collidingElements.forEach(
        collidingWall => {
            if (collidingWall instanceof SquareMapTile) {
                nSquareMapTiles += 1
            }
        }
    )
    return nSquareMapTiles;
}

class GameElements {
    initialSpeedForAccel = 1;
    velocity = new AttributeXY();
    acceleration = new AttributeXY(0.2, 0.1);
    velocityMax = new AttributeXY(5, 5);
    previousDirectionWasRight = true;

    topLeft = new AttributeXY()
    size = new AttributeXY(10, 10);
    color = "lime"

    spriteMapImg = null;
    spriteAnimationState = 0;
    spriteAnimationStates = [0, 1, 2, 1];
    spriteAnimationDelayFrame = 0;
    spriteAnimationDelayFrames = 5;

    constructor(leftX, topY) {
        this.topLeft.x = leftX
        this.topLeft.y = topY
    }

    update(game) {
    }

    draw(cvs, ctx) {
    }

    loadSpriteMapIfNotAlreadyLoaded(spriteMapFilename) {
        if (this.spriteMapImg === null) {
            this.spriteMapImg = new Image();
            this.spriteMapImg.src = "img" + "/" + spriteMapFilename
        }
    }

    drawFromSpriteMap(ctx, spriteColumn, spriteRow) {

        let destinationX = this.topLeft.x
        let destinationWidth = this.size.x

        ctx.drawImage(
            this.spriteMapImg,
            spriteColumn * this.size.x, spriteRow * this.size.y,
            this.size.x, this.size.y,
            destinationX, this.topLeft.y,
            destinationWidth, this.size.y
        )
    }

    getLeftMost(element) {
        return this.topLeft.x
    }

    getRightMost(element) {
        return this.topLeft.x + this.size.x - 1;
    }

    getUpperMost(element) {
        return this.topLeft.y;
    }

    isOnGround(game) {
        // Assume that element is on the ground if moving them one pixel down causes
        // a collision with the ground
        return isElementCollidingWithMap(new this.constructor(
            this.topLeft.x,
            this.topLeft.y + 1
        ), game).down
    }

    applyGravity() {
        if (Math.abs(this.velocity.y) < this.initialSpeedForAccel) {
            this.velocity.y = this.initialSpeedForAccel;
        } else {
            if (this.velocity.y < 0) {
                this.velocity.y *= 1 - this.acceleration.y;
            } else {
                this.velocity.y *= 1 + this.acceleration.y;
            }
        }
    }

    limitVelocity() {
        // limit x-axis velocity
        if (Math.abs(this.velocity.x) > this.velocityMax.x) {
            if (this.velocity.x > 0) {
                this.velocity.x = this.velocityMax.x
            }
            if (this.velocity.x < 0) {
                this.velocity.x = -this.velocityMax.x
            }
        }
        // limit y-axis velocity
        if (Math.abs(this.velocity.y) > this.velocityMax.y) {
            if (this.velocity.y > 0) {
                this.velocity.y = this.velocityMax.y
            } else {
                // don't limit jump velocity
            }
        }
    }

    correctForWalls(newPositionColliding, elementAtNewPosition, game) {
        if (newPositionColliding.left) {
            if (this.velocity.x < 0) {
                let isOnUpSlopeLeft = false
                newPositionColliding.collidingElements.forEach(
                    collidingElement => {
                        if (collidingElement instanceof BackSlopeMapTile) {
                            isOnUpSlopeLeft = true
                        }
                    }
                )
                if (newPositionColliding.topLeft){
                    if (getNCollidingSquareMapTiles(newPositionColliding) === 2) {
                        isOnUpSlopeLeft = false
                        this.velocity.y = 0
                    }
                }
                if (isOnUpSlopeLeft) {
                    while (newPositionColliding.any()){
                        elementAtNewPosition.topLeft.y -= 1
                        newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                    }
                } else {
                    elementAtNewPosition.topLeft.x = this.topLeft.x
                    newPositionColliding.collidingElements.forEach(
                        collidingWall => {
                            let newLeftMost = collidingWall.topLeft.x + collidingWall.size.x
                            if (newLeftMost < elementAtNewPosition.topLeft.x) {
                                elementAtNewPosition.topLeft.x = newLeftMost
                                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                            }

                            newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                            if (newPositionColliding.bottomLeft) {
                                // noinspection DuplicatedCode
                                let newBottomMost = collidingWall.topLeft.y + collidingWall.size.y - this.size.y
                                if (newBottomMost < elementAtNewPosition.topLeft.y) {
                                    elementAtNewPosition.topLeft.y = newBottomMost
                                    newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                                    this.velocity.y = 0
                                }
                            }
                        }
                    )
                    if (!newPositionColliding.down){
                        this.velocity.x = 0
                    }
                }
                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
            }
        }
        if (newPositionColliding.right) {
            if (this.velocity.x > 0) {
                let isOnUpSlopeRight = false
                newPositionColliding.collidingElements.forEach(
                    collidingElement => {
                        if (collidingElement instanceof ForwardSlopeMapTile) {
                            isOnUpSlopeRight = true
                        }
                    }
                )
                if (newPositionColliding.topRight){
                    if (getNCollidingSquareMapTiles(newPositionColliding) === 2) {
                        isOnUpSlopeRight = false
                        this.velocity.y = 0
                    }
                }
                if (isOnUpSlopeRight) {
                    while (newPositionColliding.any()){
                        elementAtNewPosition.topLeft.y -= 1
                        newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                    }
                } else {
                    elementAtNewPosition.topLeft.x = this.topLeft.x
                    newPositionColliding.collidingElements.forEach(
                        collidingWall => {
                            let newRightMost = collidingWall.topLeft.x - this.size.x
                            if (newRightMost > elementAtNewPosition.topLeft.x){
                                elementAtNewPosition.topLeft.x = newRightMost
                                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                            }

                            newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                            if (newPositionColliding.bottomRight) {
                                // noinspection DuplicatedCode
                                let newBottomMost = collidingWall.topLeft.y + collidingWall.size.y - this.size.y
                                if (newBottomMost < elementAtNewPosition.topLeft.y) {
                                    elementAtNewPosition.topLeft.y = newBottomMost
                                    newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                                    this.velocity.y = 0
                                }
                            }
                        }
                    )
                    if (!newPositionColliding.down){
                        this.velocity.x = 0
                    }
                }
                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
            }
        }
        if (newPositionColliding.down) {
            if (this.velocity.y > 0) {
                this.velocity.y = 0
                elementAtNewPosition.topLeft.y = this.topLeft.y
                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);

                while (!newPositionColliding.down){
                    elementAtNewPosition.topLeft.y += 1
                    newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
                }
                elementAtNewPosition.topLeft.y -= 1
                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
            }
        }
        if (newPositionColliding.up) {
            if (this.velocity.y < 0) {
                this.velocity.y = 0
                elementAtNewPosition.topLeft.y = this.topLeft.y
                newPositionColliding = isElementCollidingWithMap(elementAtNewPosition, game);
            }
        }
        return newPositionColliding;
    }

    isCollidingWith(element, bidirectional) {
        let elementLeftMost = element.getLeftMost(this);
        let elementRightMost = element.getRightMost(this);
        let elementUpperMost = element.getUpperMost(this);
        let elementLowerMost = element.topLeft.y + element.size.y - 1;

        let thisLeftMost = this.getLeftMost(element);
        let thisRightMost = this.getRightMost(element);
        let thisUpperMost = this.getUpperMost(element);
        let thisLowerMost = this.topLeft.y + this.size.y - 1;

        let topLeft = (
            // T
            //  E
            (elementUpperMost <= thisLowerMost) && (elementUpperMost >= thisUpperMost) &&
            (elementLeftMost <= thisRightMost) && (elementLeftMost >= thisLeftMost)
        );
        let bottomLeft = (
            //  E
            // T
            (elementLowerMost >= thisUpperMost) && (elementLowerMost <= thisLowerMost) &&
            (elementLeftMost <= thisRightMost) && (elementLeftMost >= thisLeftMost)
        )
        let bottomRight = (
            // E
            //  T
            (elementLowerMost >= thisUpperMost) && (elementLowerMost <= thisLowerMost) &&
            (elementRightMost >= thisLeftMost) && (elementRightMost <= thisRightMost)
        )
        let topRight = (
            //  T
            // E
            (elementUpperMost >= thisUpperMost) && (elementUpperMost <= thisLowerMost) &&
            (elementRightMost >= thisLeftMost) && (elementRightMost <= thisRightMost)
        )

        let collisions = new CollisionInfo();

        collisions.left = (topLeft || bottomLeft);
        collisions.right = topRight || bottomRight;
        collisions.up = topRight || topLeft;
        collisions.down = bottomRight || bottomLeft;

        collisions.topLeft = topLeft;
        collisions.bottomLeft = bottomLeft;
        collisions.bottomRight = bottomRight;
        collisions.topRight = topRight;

        if (bidirectional){
            let bidirectionalCollisions = element.isCollidingWith(this, false)
            collisions.left ||= bidirectionalCollisions.right;
            collisions.right ||= bidirectionalCollisions.left;
            collisions.up ||= bidirectionalCollisions.down;
            collisions.down ||= bidirectionalCollisions.up;

            collisions.topLeft ||= bidirectionalCollisions.bottomRight;
            collisions.bottomLeft ||= bidirectionalCollisions.topRight;
            collisions.bottomRight ||= bidirectionalCollisions.topLeft;
            collisions.topRight ||= bidirectionalCollisions.bottomLeft;
        }

        return collisions
    }
}

class Club extends GameElements {
    maxState = 15;
    maxLength = 25
    size = new AttributeXY(this.maxLength, 10);
    state = 0;
    color = "brown";

    constructor() {
        super(0, 0);
    }
}

export class TestPixel extends GameElements {
    size = new AttributeXY(1, 1);
    color = "yellow";
    collisionsColored = false

    update(game) {
        if (!this.collisionsColored) {
            if (isElementCollidingWithMap(this, game).any()) {
                this.color = "red"
            }
            this.collisionsColored = true
        }
    }
    draw(cvs, ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(this.topLeft.x, this.topLeft.y, this.size.x, this.size.y)
    }
}

export class MapTile extends GameElements {
    size = new AttributeXY(50, 50);
}

export class SquareMapTile extends MapTile {
    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("mapTileSpriteSheet.png");
        this.drawFromSpriteMap(ctx, 0, 0)
    }
}

export class ForwardSlopeMapTile extends MapTile {
    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("mapTileSpriteSheet.png");
        this.drawFromSpriteMap(ctx, 0, 1)
    }

    getLeftMost(element) {
        // if element is at top of this tile then left most is right side of tile
        // if element is at bottom of this tile then left most is left side of tile
        let leftMostMax = this.topLeft.x + this.size.x - 1;
        let leftMostMin = this.topLeft.x;

        let leftMost;
        let yTopSideOfElementRelativeToThis =
            (element.topLeft.y + element.size.y) - this.topLeft.y
        if (yTopSideOfElementRelativeToThis < 0){
            leftMost = leftMostMax; // bottom of element is above tile
        } else if(yTopSideOfElementRelativeToThis > (this.size.x - 1)){
            leftMost = leftMostMin // bottom of element is below tile
        } else {
            leftMost = this.topLeft.x + this.size.x - yTopSideOfElementRelativeToThis
        }

        if (leftMost > leftMostMax){
            leftMost = leftMostMax
        } else if (leftMost < leftMostMin){
            leftMost = leftMostMin
        }

        return leftMost
    }

    getUpperMost(element) {
        let upperMostMax = this.topLeft.y + this.size.y - 1
        let upperMostMin = this.topLeft.y

        let upperMost;
        let xRightSideOfElementRelativeToThis =
            (element.topLeft.x + element.size.x) - this.topLeft.x
        if (xRightSideOfElementRelativeToThis < 0){
            upperMost = upperMostMax // right side of element is to left of tile
        } else if(xRightSideOfElementRelativeToThis > (this.size.y - 1)){
            upperMost = upperMostMin // right side of element is to right of tile
        } else {
            upperMost = this.topLeft.y + this.size.y - xRightSideOfElementRelativeToThis
        }

        if (upperMost > upperMostMax){
            upperMost = upperMostMax
        } else if (upperMost < upperMostMin){
            upperMost = upperMostMin
        }

        return upperMost
    }
}

export class BackSlopeMapTile extends MapTile {
    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("mapTileSpriteSheet.png");
        this.drawFromSpriteMap(ctx, 0, 2)
    }

    getRightMost(element) {
        // if element is at top of this tile then right most is left side of tile
        // if element is at bottom of this tile then right most is right side of tile
        let yTopSideOfElementRelativeToThis = Math.min(
            (element.topLeft.y + element.size.y) - this.topLeft.y,
            this.size.x)
        return this.topLeft.x + yTopSideOfElementRelativeToThis - 1
    }

    getUpperMost(element) {
        let xLeftSideOfElementRelativeToThis = element.topLeft.x - this.topLeft.x
        let yUpperMost = this.topLeft.y + xLeftSideOfElementRelativeToThis
        if (xLeftSideOfElementRelativeToThis < 0) {
            yUpperMost = this.topLeft.y
        }
        return yUpperMost
    }
}

export class InnerMapTile extends SquareMapTile {
    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("mapTileSpriteSheet.png");
        this.drawFromSpriteMap(ctx, 0, 3)
    }
}

export class Gate extends GameElements {
    size = new AttributeXY(10, 40);
    color = "lime";

    draw(cvs, ctx) {
        ctx.fillStyle = this.color;
        ctx.fillRect(
            this.topLeft.x,
            this.topLeft.y,
            this.size.x,
            this.size.y
        )
        ctx.fillStyle = "red";
        ctx.fillRect(
            this.topLeft.x,
            this.topLeft.y,
            this.size.x,
            this.size.y * 0.2
        )
    }
}


export class Baddie extends GameElements {
    size = new AttributeXY(18, 12);
    paused = false
    pauseCount = 0
    maxPauseCount = 25
    startedMovingX = false
    initialVelocityX = 1
    baddieHash;

    constructor(leftX, topY, previousDirectionWasRight = true, baddieHash = undefined) {
        super(leftX, topY);

        this.previousDirectionWasRight = previousDirectionWasRight;
        if (!this.previousDirectionWasRight){
            this.initialVelocityX = -this.initialVelocityX;
        }
        this.baddieHash = baddieHash;
    }

    update(game) {
        // baddie x-axis update
        if (this.paused) {
            if (this.pauseCount === this.maxPauseCount) {
                this.paused = false;
                this.pauseCount = 0

                if (this.previousDirectionWasRight) {
                    this.velocity.x = -1
                    this.previousDirectionWasRight = false;
                } else {
                    this.velocity.x = 1
                    this.previousDirectionWasRight = true;
                }

            } else {
                this.pauseCount += 1
            }
        } else {
            if (!this.isOnGround(game)) {
                this.applyGravity();
            } else { // Baddie is on the ground
                if (!this.startedMovingX) {
                    this.startedMovingX = true
                    this.velocity.x = this.initialVelocityX
                }

                let atSquareWall = false;
                let newPositionColliding = isElementCollidingWithMap(new Baddie(
                    this.topLeft.x + this.velocity.x,
                    this.topLeft.y
                ), game, false)
                if (newPositionColliding.topLeft || newPositionColliding.topRight) {
                    atSquareWall = true;
                }

                let atEdgeOfLedge = false;
                let newPositionCollidingGround = isElementCollidingWithMap(new Baddie(
                    this.topLeft.x + this.velocity.x,
                    this.topLeft.y + this.size.x + 1  // look lower than baddie to account for slope tiles
                ), game, false)
                if (
                    (!newPositionCollidingGround.bottomRight && this.previousDirectionWasRight) ||
                    (!newPositionCollidingGround.bottomLeft && !this.previousDirectionWasRight)
                ) {
                    atEdgeOfLedge = true;
                }

               if (atEdgeOfLedge || atSquareWall) {
                   this.paused = true;
                   this.velocity.x = 0;
               }
            }
            this.limitVelocity();

            // error check move and apply it
            let baddieAtNewPosition = new Baddie(
                Math.floor(this.topLeft.x + this.velocity.x),
                Math.floor(this.topLeft.y + this.velocity.y)
            )
            let newPositionColliding = isElementCollidingWithMap(baddieAtNewPosition, game);
            newPositionColliding = this.correctForWalls(
                newPositionColliding,
                baddieAtNewPosition,
                game
            )

            // keep baddie moving in case correctForWalls stopped them
            if (this.startedMovingX && (this.velocity.x === 0)){
                if (this.previousDirectionWasRight) {
                    this.velocity.x = 1
                } else {
                    this.velocity.x = -1
                }
            }

            this.topLeft = baddieAtNewPosition.topLeft
            this.collided = newPositionColliding
        }
    }

    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("baddieSpriteSheet.png");

        let spriteRow = 0
        if (!this.previousDirectionWasRight) {
            spriteRow = 1
        }
        if (!this.paused) {
            if (this.spriteAnimationDelayFrame > this.spriteAnimationDelayFrames) {
                this.spriteAnimationState += 1
                if (this.spriteAnimationState === this.spriteAnimationStates.length) {
                    this.spriteAnimationState = 0
                }
                this.spriteAnimationDelayFrame = 0
            }
        } else {
            this.spriteAnimationState = 0
        }
        this.drawFromSpriteMap(
            ctx, this.spriteAnimationStates[this.spriteAnimationState], spriteRow
        );
        this.spriteAnimationDelayFrame += 1

    }

    static getBaddieHash(mapScreenIndex, mapRow, mapCol) {
        return `${mapScreenIndex}_${mapRow}_${mapCol}`;
    }

}

export class Player extends GameElements {
    collisionSize = 2;
    jumpVelocity = 7;
    size = new AttributeXY(16, 30);
    collided = new AttributeFourDirection();
    actionRequested = new AttributeFourDirection()
    club = new Club();
    isTakingDamageAnimationStep = 0;
    isTakingDamageAnimationStepMax = 15;

    update(game) {
        // player x-axis update from user input
        if (this.club.state === 0) {
            if (this.actionRequested.left) {
                this.previousDirectionWasRight = false;
                if (Math.abs(this.velocity.x) < this.initialSpeedForAccel) {
                    this.velocity.x = -this.initialSpeedForAccel
                } else {
                    if (this.velocity.x < 0) {
                        this.velocity.x *= 1 + this.acceleration.x;
                    } else {
                        this.velocity.x *= 1 - this.acceleration.x;
                    }
                }
            } else if (this.actionRequested.right) {
                this.previousDirectionWasRight = true;
                if (Math.abs(this.velocity.x) < this.initialSpeedForAccel) {
                    this.velocity.x = this.initialSpeedForAccel
                } else {
                    if (this.velocity.x > 0) {
                        this.velocity.x *= 1 + this.acceleration.x;
                    } else {
                        this.velocity.x *= 1 - this.acceleration.x;
                    }
                }
            } else if (!this.actionRequested.left && !this.actionRequested.right) {
                if (Math.abs(this.velocity.x) > this.initialSpeedForAccel) {
                    this.velocity.x *= 1 - this.acceleration.x;
                } else {
                    this.velocity.x = 0;
                }
            }
        }

        if (game.gravity) {
            if (!this.isOnGround(game)) {
                this.applyGravity()
            } else {  // Player is on the ground
                if (this.actionRequested.up) {  // Jump is requested
                    if (this.club.state === 0) {
                        this.velocity.y = -this.jumpVelocity;
                        playAudio(AudioEnum.JUMP)
                    }
                }
                if (this.actionRequested.down) { // Club is requested
                    if (this.club.state === 0) {
                        if (this.velocity.x === 0) {
                            this.club.state = 1;
                            playAudio(AudioEnum.CLUB)
                        }
                    }
                }
            }
        } else {  // no gravity move up and down for debug
            if (this.actionRequested.up) {
                this.topLeft.y -= 1
            }
            if (this.actionRequested.down) {
                this.topLeft.y += 1
            }
        }
        this.limitVelocity();

        // advance the club state
        if (this.club.state !== 0) {
            this.club.state += 1
            if (this.club.state > this.club.maxState) {
                this.club.state = 0;
            }
        }

        // scroll to previous map screen on left
        let cvsWidth = game.mapScreens[0].mapRows[0].length * (
            new MapTile(0,0).size.x)
        if ((Math.floor((this.topLeft.x) / 50)) < 0) {
            if (game.mapScreenIndex > 0) {
                this.velocity.y = 0
                game.setMapScreenIndex(game.mapScreenIndex - 1, false, false);
                this.topLeft.x = cvsWidth - this.size.x;
            } else {
                if (this.velocity.x < 0) {  // don't move further left than last map screen
                    this.velocity.x = -(this.velocity.x - 1);
                    game.messageGeneral = "Go right to advance..."
                    playAudio(AudioEnum.WHISTLE)
                }
            }
        }
        // scroll to next map screen on right
        let currentMapScreen = game.mapScreens[game.mapScreenIndex]
        if (Math.floor(((this.topLeft.x + (this.size.x / 2)) / 50) + 1) > currentMapScreen.mapRows[0].length) {
            let scroll = true;
            let nextMapScreen = game.mapScreenIndex + 1;
            if (game.mapScreens[game.mapScreenIndex].hasRequiredScoreToAdvance) {
                if (game.score < currentMapScreen.requiredScoreToAdvance) {
                    scroll = false;  // don't move further right if score is less than required
                    game.messageGeneral = `Score of ${currentMapScreen.requiredScoreToAdvance} ` +
                        'is required to advance...'
                } else if (!isElementCollidingWithGate(game.player, game).any()) {
                    scroll = false;  // don't move further right if not colliding with Gate
                    game.messageGeneral = "Go through the Gate to advance..."
                } else {
                    if (game.lastGateMapScreenIndex < nextMapScreen) { // only first time through gate
                        game.lastGateMapScreenIndex = nextMapScreen;
                        game.lastGateScore = game.score;
                        game.lastGateBaddiesClubbed = game.baddiesClubbed.slice(); // use slice to get new array
                    }
                }
            }

            if (scroll) {
                if (game.mapScreenIndex === (game.mapScreens.length - 1)) { // this is the last map screen
                    // don't move further right than last map screen
                    game.isComplete = true;
                } else {  // this is not the last map screen
                    this.velocity.y = 0
                    game.setMapScreenIndex(nextMapScreen, false);
                    this.topLeft.x = 0;
                }
            } else {
                if (this.velocity.x > 0) {
                    this.velocity.x = -this.velocity.x;
                    playAudio(AudioEnum.WHISTLE)
                }
            }
        }

        // error check move and apply it
        let playerAtNewPosition = new Player(
            Math.floor(this.topLeft.x + this.velocity.x),
            Math.floor(this.topLeft.y + this.velocity.y)
        )
        let newPositionColliding = isElementCollidingWithMap(playerAtNewPosition, game);
        if (game.walls) {
            newPositionColliding = this.correctForWalls(
                newPositionColliding,
                playerAtNewPosition,
                game
            );
        }

        this.topLeft = playerAtNewPosition.topLeft
        this.collided = newPositionColliding

        // update club location
        this.club.size.x = (this.club.maxLength * (
            2 * this.club.state / this.club.maxState));
        if (this.club.state > (this.club.maxState / 2)) {
            this.club.size.x = (2 * this.club.maxLength) - this.club.size.x
        }
        if (this.previousDirectionWasRight) {
            this.club.topLeft.x = this.topLeft.x + this.size.x
        } else {
            this.club.topLeft.x = this.topLeft.x - this.club.size.x;
        }
        this.club.topLeft.y = this.topLeft.y + this.size.y - this.club.size.y


        if (this.club.state !== 0) {
            let clubCollidingBaddies = isElementCollidingWithBaddie(this.club, game);
            if (clubCollidingBaddies.any()) {
                clubCollidingBaddies.collidingElements.forEach(
                    collidingBaddie => {
                        game.elements.splice(
                            game.elements.indexOf(collidingBaddie), 1)
                        game.score += 1
                        game.baddiesClubbed.push(collidingBaddie.baddieHash)
                        playAudio(AudioEnum.CLUBBED)
                    }
                )
            }
        }

        // when isTakingDamageAnimationStep != 0 player is taking damage
        if (isElementCollidingWithBaddie(playerAtNewPosition, game).any()) {
            game.health -= 1;

            function resetTheGame() {
                game.reset()
            }

            if (game.health <= 0) {
                playAudio(AudioEnum.OVER, resetTheGame)
            } else {
                if (this.isTakingDamageAnimationStep === 0) {  // first damage
                    this.isTakingDamageAnimationStep = 1
                    playAudio(AudioEnum.DAMAGE)
                } else {  // subsequent damage
                    this.isTakingDamageAnimationStep += 1
                    if (this.isTakingDamageAnimationStep > this.isTakingDamageAnimationStepMax) {
                        this.isTakingDamageAnimationStep = 1
                    }
                    playAudio(AudioEnum.DAMAGE)
                }
            }
        } else {
            this.isTakingDamageAnimationStep = 0
        }
    }

    draw(cvs, ctx) {
        this.loadSpriteMapIfNotAlreadyLoaded("playerSpriteSheet.png");

        // draw player body
        let spriteRow = 0
        if (this.velocity.y !== 0){
            spriteRow = 3
            if (!this.previousDirectionWasRight) {
                spriteRow = 4
            }
            this.spriteAnimationState = 0
        } else if (this.isTakingDamageAnimationStep === 0) { // not taking damage
            if (!this.previousDirectionWasRight) {
                spriteRow = 1
            }
            if (this.velocity.x !== 0) {
                if (this.spriteAnimationDelayFrame > this.spriteAnimationDelayFrames) {
                    this.spriteAnimationState += 1
                    if (this.spriteAnimationState === this.spriteAnimationStates.length) {
                        this.spriteAnimationState = 0
                    }
                    this.spriteAnimationDelayFrame = 0
                }
            } else {
                this.spriteAnimationState = 0
            }
        } else {
            spriteRow = 2
            if (this.spriteAnimationDelayFrame > this.spriteAnimationDelayFrames) {
                this.spriteAnimationState += 1
                if (this.spriteAnimationState === this.spriteAnimationStates.length) {
                    this.spriteAnimationState = 0
                }
                this.spriteAnimationDelayFrame = 0
            }
        }
        this.drawFromSpriteMap(
            ctx, this.spriteAnimationStates[this.spriteAnimationState], spriteRow
        );
        this.spriteAnimationDelayFrame += 1

        // draw collisions
        ctx.fillStyle = "yellow";
        if (this.collided.right) {
            ctx.fillRect(
                this.topLeft.x + this.size.x - this.collisionSize,
                this.topLeft.y,
                this.collisionSize,
                this.size.y
            )
        }
        if (this.collided.left) {
            ctx.fillRect(
                this.topLeft.x,
                this.topLeft.y,
                this.collisionSize,
                this.size.y
            )
        }
        if (this.collided.up) {
            ctx.fillRect(
                this.topLeft.x,
                this.topLeft.y,
                this.size.x,
                this.collisionSize
            )
        }
        if (this.collided.down) {
            ctx.fillRect(
                this.topLeft.x,
                this.topLeft.y + this.size.y - this.collisionSize,
                this.size.x,
                this.collisionSize
            )
        }

        // draw club
        if (this.club.state !== 0) {
            ctx.fillStyle = this.club.color;
            ctx.fillRect(
                this.club.topLeft.x,
                this.club.topLeft.y,
                this.club.size.x,
                this.club.size.y
            )
        }
    }
}

function isElementCollidingWithMap(element, game, bidirectional = true) {
    return isElementCollidingWithX(element, MapTile, game, bidirectional)
}

function isElementCollidingWithBaddie(element, game, bidirectional = true) {
    return isElementCollidingWithX(element, Baddie, game,  bidirectional)
}

function isElementCollidingWithGate(element, game, bidirectional = true) {
    return isElementCollidingWithX(element, Gate, game,  bidirectional)
}

function isElementCollidingWithX(element, classOfX, game, bidirectional) {
    let collisions = new CollisionInfo();

    let elementsOfClass = game.elements.filter(
        element => (element instanceof classOfX))
    elementsOfClass.forEach(elementOfClass => {
        if (elementOfClass.isCollidingWith(element, bidirectional).left) {
            collisions.left = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).right) {
            collisions.right = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).up) {
            collisions.up = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).down) {
            collisions.down = true;
            collisions.collidingElements.add(elementOfClass)
        }

        if (elementOfClass.isCollidingWith(element, bidirectional).topLeft) {
            collisions.topLeft = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).bottomLeft) {
            collisions.bottomLeft = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).bottomRight) {
            collisions.bottomRight = true;
            collisions.collidingElements.add(elementOfClass)
        }
        if (elementOfClass.isCollidingWith(element, bidirectional).topRight) {
            collisions.topRight = true;
            collisions.collidingElements.add(elementOfClass)
        }
    })

    return collisions
}

class CollisionInfo extends AttributeFourDirection {
    constructor() {
        super()
        this.topLeft = false;
        this.bottomLeft = false;
        this.bottomRight = false;
        this.topRight = false;
        this.collidingElements = new Set();
    }

    any(){
        return super.any() ||
            this.topLeft || this.bottomLeft || this.bottomRight || this.topRight
    }
}
