"use strict";

class Map {
    mapRows;
    requiredScoreToAdvance;
    mapXDimensionLength = 16;
    mapYDimensionLength = 12;
    constructor(mapRows, requiredScoreToAdvance = undefined) {
        this.mapRows = mapRows;
        this.requiredScoreToAdvance = requiredScoreToAdvance;
        this.validateMapRows(mapRows);
    }

    get hasRequiredScoreToAdvance() {
        return this.requiredScoreToAdvance !== undefined
    }

    get hasGateInMapRows() {
        return this.mapRows.join().indexOf("g") !== -1
    }

    getGround(column){
        let lastEmptyRowIndex = 0;
        for (let rowIndex = 0; rowIndex < this.mapRows.length; rowIndex++) {
            let mapCell = this.mapRows[rowIndex][column]
            if (" sg".indexOf(mapCell) !== -1){
                lastEmptyRowIndex = rowIndex;
            }
        }
        return (lastEmptyRowIndex + 1) * 50
    }

    validateMapRows(mapRows) {
        for (const mapRow of this.mapRows) {
            if (mapRow.length !== this.mapXDimensionLength) {
                throw new Error(`Map X dimension must be ${this.mapXDimensionLength} not '${mapRow}'`);
            }
        }

        if (this.mapRows.length !== this.mapYDimensionLength) {
            throw new Error(`Map Y dimension must be ${this.mapYDimensionLength} not \n${mapRows.join('\n')}`);
        }

        if (this.hasRequiredScoreToAdvance && !this.hasGateInMapRows) {
            throw new Error('Maps with Required Score to Advance must have a Goal Map Tile')
        }

        if (this.hasGateInMapRows && !this.hasRequiredScoreToAdvance) {
            throw new Error('Maps with a Goal Map Tile must have Required Score to Advance')
        }
    }
}


// NOTE: Maps screens must have continuous ground levels to prevent player from going into a wall
export let mapScreens = [
    new Map(
    [
        "□□□□□□□□□□□□□□□□",
        " □□□□□□□□□□     ",
        " □□□□□          ",
        " □□□□           ",
        "    □S          ",
        "■■  □■          ",
        "□□■   ■   S     ",
        "□□□■     ■■  ■  ",
        "□□□□■      ■s   ",
        "□□□□□↘   ■  ↗↘  ",
        "□□□□□□↘ ■□s↗□□■g",
        "□□□□□□□■□□■□□□□■"
    ], 4),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "  □□□□□    □□   ",
        "   □□           ",
        "                ",
        "                ",
        "                ",
        "          s     ",
        "   s      ■■    ",
        "  s■S    ■□     ",
        "  ↗□↘ s■        ",
        " ↗□□□↘↗□■■↘s↗■s ",
        "■□□□□□□□□□□■□□■■"
    ]),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "  □□□□□    □□   ",
        "   □□           ",
        "                ",
        " ↗ ↘            ",
        " s    s         ",
        " ■   ■■         ",
        "    ↗□□↘        ",
        "   ↗□□□□↘       ",
        "  ↗□□□□□□↘   s  ",
        " ↗□□□□□□□□↘ ↗■■g",
        "■□□□□□□□□□□■□□□■"
    ], 14),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "□    □□□□□    □□",
        "                ",
        "                ",
        "                ",
        "                ",
        "                ",
        "                ",
        "                ",
        "    S   s       ",
        " ↗■■■■  ■■↘s↗■↘ ",
        "■□□□□□■■□□□□□□□■"
    ]),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "□    □ □□□    □ ",
        "        □       ",
        "                ",
        "                ",
        "                ",
        "                ",
        "       S  ↗    g",
        "    S↗■■■■□  ■■■",
        "  s↗□□□□□□□s↗□□□",
        " ↗□□□□□□□□□■□□□□",
        "■□□□□□□□□□□□□□□□"
    ], 21),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "□□   □□□□□    □□",
        "□      □□□   □□□",
        "                ",
        "                ",
        "                ",
        "                ",
        "         s  s   ",
        "■    S↗■■■■ ■↘  ",
        "□■ S↗■□□□□□■□□■■",
        "□□■■□□□□□□□□□□□□",
        "□□□□□□□□□□□□□□□□"
    ]),
    new Map([
        "□□□□□□□□□□□□□□□□",
        "□□   □□ □□    □□",
        "□     □□□      □",
        "                ",
        "                ",
        "                ",
        "                ",
        "        ↗■ s    ",
        "     S ■□□S■s↘ g",
        "■■ S↗■■□□□■□■□■■",
        "□□■↗□□□□□□□□□□□□",
        "□□□□□□□□□□□□□□□□"
    ], 30),
];

mapScreens.validateMapScreens = function (){
    this.forEach((currentMapScreen, currentMapScreenIndex) => {
        if ((currentMapScreenIndex > 0) && (currentMapScreenIndex < (this.length - 1))){
            let previousMapScreen = mapScreens[currentMapScreenIndex - 1];
            let nextMapScreen = mapScreens[currentMapScreenIndex + 1];

            let mapXDimensionMaxIndex = currentMapScreen.mapXDimensionLength - 1

            if (currentMapScreen.getGround(0) !== previousMapScreen.getGround(mapXDimensionMaxIndex)){
                throw new Error(`Map screen index ${currentMapScreenIndex} first column isn't continuous`);
            }
            if (currentMapScreen.getGround(mapXDimensionMaxIndex) !== nextMapScreen.getGround(0)){
                throw new Error(`Map screen index ${currentMapScreenIndex} last column isn't continuous`);
            }
        }
    })
}
